import React from "react";
import { View, StyleSheet, Text, ScrollView, ImageBackground, TouchableOpacity,Modal,SafeAreaView,Platform,ActivityIndicator,Linking } from 'react-native';
import { StatusBar, Loader } from '../components/GeneralComponents';
import {  Icon,Fab,Toast,Button as Btn } from "native-base";
import Dialog from "react-native-dialog";
import * as colors from '../assets/css/Colors';
import { connect } from 'react-redux';
import axios from 'axios';
import { img_url, api_url, service,quick_order,address_list,promo_code,getrider,deleteorder} from '../config/Constants';
import DropDownPicker from 'react-native-dropdown-picker';
import { Avatar } from 'react-native-elements';
import { updateSocketStatus , updateSearchStatus , addOrderData ,addRiderData ,resetOthers } from '../actions/OthersActions';
class RiderInfoComponent extends React.Component {
    constructor(props) {
    super(props);

    this.state = {
        isLoding:false,
        isLoading :false,



    }
  }



  handleCancel = async () => {
      this.setState({dialogVisible:false,newAddress:false})
  }

  updateStarch = (value)=>{

  }

  closeModal = ()=>{
      this.setState({dialogVisible:false,newAddress:false})

  }
  closeModalSuccess = ()=>{
      this.setState({dialogVisiblesuccess:false})

  }

  updateShirtboard = (value)=>{
      let newValue = 0
      if(value == 'Ten (10) shirt board'){
          newValue = 1000
      }else if (value == 'Maximum of 30 shirt boards') {
          newValue = 2000
      }else{
         newValue = 0
      }
      let arrayHolder = {
          type:'shirt board',
          value:value,
          price: newValue,
      }

      this.setState({shirtboardvalue:arrayHolder})

  }

  showQuickModal = async () => {
      this.setState({loading:true})
      //this.setState({ dialogVisible: true })
      await axios({
        method: 'post',
        url: api_url + address_list,
        headers: {
          'x-access-token': global.access_token,
          'Content-Type': 'application/json'
      },
        data:{ customer_id: global.id},

      })
      .then(async response => {



          if(response.data.result.length > 0){
              // user has address
              console.log('has addres',response.data.result)
              let arrayHolder = []

              response.data.result.map((data) => {

                  arrayHolder.push({label:data.address,value:data.id})
                  //console.log(data.code);
              });
              let arrayHolder2 = []
              await axios({
                method: 'post',
                url: api_url + promo_code,
                headers: {
                  'x-access-token': global.access_token,
                  'Content-Type': 'application/json'
              },
                data:{ customer_id: global.id},

              })
              .then(async response => {

                  console.log(response)

                  if(response.data.result.length > 0){
                      // user has address
                      console.log('has addres',response.data.result)


                      response.data.result.map((data) => {

                          arrayHolder2.push({label:data.promo_name,value:data.id})
                          //console.log(data.code);
                      });

                      this.setState({discountactive:true})

                  }
                  //this.props.navigation.navigate('Orders');
                  // go to order
              })
              .catch(async error => {
                  // show snackbar here
                  Toast.show({
                          text: "Something went wrong please try again later",
                          duration: 3000,
                          position: "bottom"
                        })
                  console.log(error)
                  await this.setState({loading:false,dialogVisible:false})
              });

              this.setState({dialogVisible:true,addres:arrayHolder,promo:arrayHolder2,loading:false})

          }else{
              console.log('test this should be error',response.data)
              await this.setState({loading:false,newAddress:true,dialogVisible:true})

          }
          //this.props.navigation.navigate('Orders');
          // go to order
      })
      .catch(async error => {
          // show snackbar here
          Toast.show({
                  text: "Something went wrong please try again later",
                  duration: 3000,
                  position: "bottom"
                })
          console.log('this should be error',error)
          await this.setState({loading:false,dialogVisible:false,newAddress:false})
      });


  }

  createAddress = async () => {
     this.setState({newAddress:false,dialogVisible:false})
     this.props.navigation.navigate('AddressList',{ from:'more' });
  }

  placeOrder = async () => {
      //check for restrictions b4 we process
      if(this.state.starchvalue.length < 1 || this.state.shirtboardvalue.length < 1 || this.state.addressvalue.length < 1){
          Toast.show({
                  text: "Sorry Please all options must be selected ",
                  duration: 3000,
                  position: "top",
                  type:"danger"
                })
          return
      }
    let orderOtherArray= [];
    orderOtherArray.push(this.state.shirtboardvalue)
    orderOtherArray.push(this.state.starchvalue)
    console.log('quickorder',{ others:JSON.stringify(Object.values(orderOtherArray)), address_id:this.state.addressvalue, promo_id:this.state.promovalue,
    })

    this.setState({loading:true})
    await axios({
      method: 'post',
      url: api_url + quick_order,
      headers: {
        'x-access-token': global.access_token,
        'Content-Type': 'application/json'
    },
    data:{ others:JSON.stringify(Object.values(orderOtherArray)), address_id:this.state.addressvalue, promo_id:this.state.promovalue,
    }})
    .then(async response => {
        await this.setState({loading:false,dialogVisible:false})
        console.log(response)
        this.getRider(response.data.result.id)
        // go to order
    })
    .catch(async error => {
        // show snackbar here
        Toast.show({
                text: "Something went wrong please try again later",
                duration: 3000,
                position: "bottom"
              })
        console.log(error)
        await this.setState({loading:false,dialogVisible:false})
    });
  }

  successModal = (status) =>{

      let that = this;

      if(parseInt(this.props.searchStatus) >= 1){


      }else{


          console.log("i dont know why its not working2", this.props.searchStatus)
      }
      return (


          <View style={styles.modalView}>

          {parseInt(this.props.searchStatus) == 1 ?
              <View>
              <Text style={{textAlign:"center"}}> Searching For Rider </Text>
              <ActivityIndicator size="large" color={colors.theme_bg} style={{marginTop: 7}}/>
              </View>
              :null}
              {parseInt(this.props.searchStatus) >= 2 ?
              <View>
              <View  style={{flexDirection:'row', flexWrap:'wrap'}}>




                      <View style={styles.gridcontain}>

                      <View style={styles.gridcontainleft}>

                       <Text style={styles.riderInfoHeader}> Your Rider </Text>
                        <Text style={styles.riderInfo}>
                            {
                                this.props.riderData[0].rider_name
                            }

                        </Text>

                        <Text style={styles.riderInfoHeader}> Plate Number </Text>
                         <Text style={styles.riderInfo}>
                             {
                                this.props.riderData[0].bike_number
                             }
                         </Text>

                      </View>

                      <View style={styles.gridcontainright}>


                        <Avatar

                        rounded
                        size="large"
                        source={{
                            uri:img_url+this.props.riderData[0].rider_image,

                        }}
                        />

                      </View>

                      </View>


              </View>

              { parseInt(this.props.searchStatus) == 2 ?
              <View>
              <Text style={{textAlign:"center"}}> Waiting For Rider to Accept order </Text>
              <ActivityIndicator size="large" color={colors.theme_bg} style={{marginTop: 7}}/>
              </View> :null
          }


              {parseInt(this.props.searchStatus) >= 3 ?
              <View style={styles.gridcontaincontact}>
                  <View style={{alignItems:"center"}}>
                      <Avatar
                      activeOpacity={0.7}
                       icon={{name: 'phone', color: '#1BBFEE', type: 'font-awesome'}}
                      onPress={() => {
                                Linking.openURL(
                                  `tel:${this.props.riderData[0].number}`
                                );
                              }}

                      rounded
                      size="large"

                      />
                      <Text> Call Rider </Text>
                  </View>

                  <View style={{alignItems:"center"}}>
                      <Avatar

                      onPress={() => {
                                Linking.openURL(
                                  `tel:08093909000`
                                );
                              }}
                      rounded
                      size="large"
                      icon={{name: 'phone', color: '#1BBFEE', type: 'font-awesome'}}
                      />
                      <Text> Call SqueakyKlin </Text>
                  </View>

                  <View style={{alignItems:"center"}}>
                  {this.state.isLoading ?
                      <ActivityIndicator  color={colors.theme_bg} size="large"  style={{marginTop:30}} /> :
                      <Avatar
                      onPress={() => {
                                this.deleteorder(this.props.orderData)
                              }}

                      rounded
                      size="large"
                      icon={{name: 'close', color: '#1BBFEE', type: 'font-awesome'}}
                      />}
                      <Text> Cancle Order  </Text>
                  </View>
            </View>:null}

            {parseInt(this.props.searchStatus) >= 3 ? <View>
            <Btn
              onPress={this.close}
              style={styles.buttonFilled}>
             <Text style={styles.buttonFilledText}>Close </Text>
            </Btn>
            </View>:null}

              </View>
              :null
              }




          </View>


      )
  }


  close = async () =>{
      this.setState({isLoding:false})
      this.props.updateSearchStatus(0)


    }


  deleteorder = async (order_id) => {

      this.setState({isLoading:true})

      await axios({
        method: 'post',
        url: api_url + deleteorder+"?id="+order_id[0].id,
        headers: {
          'x-access-token': global.access_token,
          'Content-Type': 'application/json'
      }
        //data:{ customer_id: global.id, payment_mode: paymentMode, address_id:this.props.address, expected_delivery_date:this.props.delivery_date, total:this.props.total, discount:this.props.discount, sub_total:this.props.sub_total, promo_id:this.props.promo_id, items: JSON.stringify(Object.values(this.props.items)) }
      })
      .then(async response => {
          this.setState({isLoading:false})
        if(response.data.status == 1){


            this.showSnackbar('Your order have been cancled','success')
            this.close();
        }else{
            //close modal
            this.showSnackbar('Something went wrong please try again','danger')


            //show flash message
        }

      })
      .catch(error => {
          this.setState({isLoading:false})
          this.showSnackbar('Something went wrong please try again','danger')

       //close modal
      });
  }

  showSnackbar = (msg,type) =>{
      Toast.show({
              text: msg,
              duration: 3000,
              position: "top",
              type: type
            })
  }


    render() {

      return (

    <View>
            {this.successModal(true)}

       </View>



      );
   }
}

function mapStateToProps(state){
  return{
    searchStatus : state.others.searchstatus,
    orderData : state.others.orderdata,
    riderData : state.others.riderdata,



  };
}

const mapDispatchToProps = (dispatch) => ({
    updateSearchStatus: (data) => dispatch(updateSearchStatus(data)),

});


export default connect(mapStateToProps,mapDispatchToProps)(RiderInfoComponent);


const styles = StyleSheet.create({
    buttonFilled:{
        backgroundColor:colors.theme_bg,
        borderRadius: 10,
        height:40,
        width:'100%',
        marginTop:10,
        marginBottom:10,
        justifyContent:'center',
    },
    buttonFilledText:{
        alignSelf:'center',
        color:'#fff'
    },
    gridcontain: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: '2%',
        marginBottom: '6%',
        //alignItems:'center',
    },
    gridcontaincontact: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: '2%',
        marginBottom: '6%',
        borderBottomWidth:1,
        borderTopWidth:1,
        borderColor:"#ccc",
        paddingBottom:10
    },

    gridcontainleft: {



    },
    gridcontainright: {
        width: 100
    },
    riderInfoHeader:{
        fontSize:18,
        textAlign:"center",
        marginBottom:5,
        color:"#000"
    },
    riderInfo:{
        fontSize:22,
        textAlign:"center",
        marginBottom:5,
        color:"#1BBFEE"
    },
    modalView: {
      backgroundColor: '#fff',
      position: 'absolute',
      bottom: 0,
      width:"100%",
      minHeight:200,
      paddingHorizontal:10,

      paddingVertical:10,
      marginBottom:10,
      borderRadius:10,
      shadowColor: "#000",
      shadowOffset: {
          width: 0,
          height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,

      elevation: 5,


    },

});
