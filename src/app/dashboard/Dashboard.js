import React, { Component } from 'react';
// import { Line, Doughnut, Bar, Radar } from 'react-chartjs-2';
import { Line, Bar, Radar } from 'react-chartjs-2';
import { Link} from 'react-router-dom';

import { ProgressBar, Dropdown } from 'react-bootstrap';
import GaugeChart from 'react-gauge-chart';
import { VectorMap } from "react-jvectormap"
import Spinner from '../shared/Spinner';
import axios from 'axios';


// import DatePicker from 'react-datepicker';
// import { Dropdown } from 'react-bootstrap';

export class Dashboard extends Component {



  constructor(props) {
    super(props);
    this.state = {
        firstgeneration:"",
        secondgeneration:"",
        thirdgeneration:"",
        fourthgeneration:"",
        fifthgeneration:"",
        firstGenerationData:[],
		isloading:false,
		teammateloader:false,
    }

  }



  componentDidMount(){
      const TOKEN = localStorage.getItem('token')
      let url = "https://api.adloyaltybn.com/site"
	  this.setState({isloading:true})
      axios({
        method: 'get',
        url: url,
        headers: {
          'x-access-token': TOKEN,
          'Content-Type': 'application/json'
      }
        //data:{ customer_id: global.id, payment_mode: paymentMode, address_id:this.props.address, expected_delivery_date:this.props.delivery_date, total:this.props.total, discount:this.props.discount, sub_total:this.props.sub_total, promo_id:this.props.promo_id, items: JSON.stringify(Object.values(this.props.items)) }
      })
       .then(res => {
           if(res.data.status == 1){
			   this.setState({isloading:false})
              this.setState({
                  firstgeneration:res.data.result.first,
                  secondgeneration:res.data.result.second,
                  thirdgeneration:res.data.result.third,
                  fourthgeneration:res.data.result.fourth,
                  fifthgeneration:res.data.result.fifth,
				  
              })
           }else{
               //this.setState({refName:res.data.result.l_name+" "+res.data.result.f_name,refId:res.data.result.id,gen:res.data.result.id+","+res.data.result.gen})
           }
       })


       axios({
         method: 'get',
         url: url+"/getteammate",
         headers: {
           'x-access-token': TOKEN,
           'Content-Type': 'application/json'
       }
         //data:{ customer_id: global.id, payment_mode: paymentMode, address_id:this.props.address, expected_delivery_date:this.props.delivery_date, total:this.props.total, discount:this.props.discount, sub_total:this.props.sub_total, promo_id:this.props.promo_id, items: JSON.stringify(Object.values(this.props.items)) }
       })
        .then(res => {
            console.log("this is me ", res.data)
            if(res.data.status == 1){

               this.setState({
                   firstGenerationData:res.data.result,
               })
            }else{
                //this.setState({refName:res.data.result.l_name+" "+res.data.result.f_name,refId:res.data.result.id,gen:res.data.result.id+","+res.data.result.gen})
            }
        })
  }


  render () {
    return (
		<>
		{this.state.isloading ?
			<Spinner /> :
      <div>
		
		
        <div className="row proBanner">
          <div className="col-12">
            <div className="card">
                <div className="card-body" style={{background:"black",color:"#eb9e51",textAlign:"center"}}>
                    <h4>Motto: Adloyalty Business Network ...creating wealth, empowering people!</h4>
                    <h5>REF LINK : {"https://portal.adloyaltybn.com/"+localStorage.getItem("reff")}</h5>
                </div>
              </div>


          </div>
        </div>
        <div className="row page-title-header">
          <div className="col-12">
            <div className="page-header" style={{marginTop:20}}>
              <h2 >Dashboard</h2>
            </div>
          </div>

        </div>
        <div className="row">
          <div className="col-md-12 grid-margin">
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col-xl-3 col-lg-6 col-sm-6 grid-margin-xl-0 grid-margin">
                    <div className="d-flex">
                      <div className="wrapper">
                        <h3 className="mb-0 font-weight-semibold">{this.state.firstgeneration}</h3>
                        <h5 className="mb-0 font-weight-medium text-primary">First Generation</h5>
                      </div>

                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-6 col-sm-6 mt-md-0 mt-4 grid-margin-xl-0 grid-margin">
                    <div className="d-flex">
                      <div className="wrapper">
                        <h3 className="mb-0 font-weight-semibold">{this.state.secondgeneration}</h3>
                        <h5 className="mb-0 font-weight-medium text-primary">Second Generation</h5>
                      </div>

                    </div>
                  </div>
                  <div className="col-xl-2 col-lg-6 col-sm-6 mt-md-0 mt-4 grid-margin-xl-0 grid-margin">
                    <div className="d-flex">
                      <div className="wrapper">
                        <h3 className="mb-0 font-weight-semibold">{this.state.thirdgeneration}</h3>
                        <h5 className="mb-0 font-weight-medium text-primary">Third Generation</h5>

                      </div>

                    </div>
                  </div>

                  <div className="col-xl-2 col-lg-6 col-sm-6 mt-md-0 mt-4 grid-margin-xl-0 grid-margin">
                    <div className="d-flex">
                      <div className="wrapper">
                        <h3 className="mb-0 font-weight-semibold">{this.state.fourthgeneration}</h3>
                        <h5 className="mb-0 font-weight-medium text-primary">Fourth Generation</h5>

                      </div>

                    </div>
                  </div>

                  <div className="col-xl-2 col-lg-6 col-sm-6 mt-md-0 mt-4 grid-margin-xl-0 grid-margin">
                    <div className="d-flex">
                      <div className="wrapper">
                        <h3 className="mb-0 font-weight-semibold">{this.state.fifthgeneration}</h3>
                        <h5 className="mb-0 font-weight-medium text-primary">Fifth Generation</h5>

                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title mb-0">First Level Downline</h4>
				{this.state.teammateloader ?
				<div className="d-xl-flex flex-column flex-lg-row">
					 <Spinner />
					 </div> :
				
                <div className="d-xl-flex flex-column flex-lg-row">
                  {
                      this.state.firstGenerationData.length > 0 ?
                      <div className="table-responsive">
                        <table className="table">
                          <thead>
                            <tr>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Mobile No</th>
                              <th>Action</th>
                              <th>REF Link</th>
                            </tr>
                          </thead>
                          <tbody>
                      {this.state.firstGenerationData.map((value, index) => (


                                <tr key={index}>
                                  <td>{value.l_name+" "+value.l_name}</td>
                                  <td>{value.email}</td>
                                  <td>{value.phones}</td>
                                  <td><Link to={"/downlinedetails/"+value.id}><label className="badge badge-primary">View</label></Link></td>
                                  <td>{"http://portal.adloyalty.com/"+value.ref_code}</td>

                                </tr>


                  ))}
                  </tbody>
                  </table>
                </div>
                  :
                  null
              }

                </div>
				}
				
				 


            </div>
            </div>
          </div>

        </div>
	

      </div>
			}
			</>
    );
  }
}
export default Dashboard;
