import React, { Component } from 'react';
// import { Line, Doughnut, Bar, Radar } from 'react-chartjs-2';
import { Line, Bar, Radar } from 'react-chartjs-2';

import { ProgressBar, Dropdown } from 'react-bootstrap';
import GaugeChart from 'react-gauge-chart';
import { VectorMap } from "react-jvectormap"
import axios from 'axios';
import { Link} from 'react-router-dom';
import Spinner from '../shared/Spinner';


// import DatePicker from 'react-datepicker';
// import { Dropdown } from 'react-bootstrap';

export class Downline extends Component {



  constructor(props) {
    super(props);
    this.state = {
        firstgeneration:"",
        secondgeneration:"",
        thirdgeneration:"",
        fourthgeneration:"",
        fifthgeneration:"",
        generationData:[],
        generationText:"",
		isloading:false,
		
    }

  }


  componentDidUpdate(prevProps) {
      const { match: { params: { downline } } } = this.props
      if (prevProps.match.params.downline !== downline){
		  this.setState({isloading:true})
          let route = downline
          switch (route) {
              case "0":
                  this.setState({generationText:"First Generation"})
                  break;
              case "1":
                  this.setState({generationText:"Second Generation"})
                  break;
              case "2":
                  this.setState({generationText:"Third Generation"})
                  break;
              case "3":
                  this.setState({generationText:"Fourth Generation"})
                  break;
              case "4":
                  this.setState({generationText:"Fifth Generation"})
                  break;
              default:
                this.setState({generationText:"First Generation"})

          }
          const TOKEN = localStorage.getItem('token')
          let url = "https://api.adloyaltybn.com/site"
           axios({
             method: 'get',
             url: url+"/getteammate?downline="+route,
             headers: {
               'x-access-token': TOKEN,
               'Content-Type': 'application/json'
           }
             //data:{ customer_id: global.id, payment_mode: paymentMode, address_id:this.props.address, expected_delivery_date:this.props.delivery_date, total:this.props.total, discount:this.props.discount, sub_total:this.props.sub_total, promo_id:this.props.promo_id, items: JSON.stringify(Object.values(this.props.items)) }
           })
            .then(res => {
			   this.setState({isloading:false})
                if(res.data.status == 1){

                   this.setState({
                       generationData:res.data.result,
                   })
                }else{
                    //this.setState({refName:res.data.result.l_name+" "+res.data.result.f_name,refId:res.data.result.id,gen:res.data.result.id+","+res.data.result.gen})
                }
            })
      }
    }

  componentDidMount(){
      let route = this.props.match.params.downline
	  this.setState({isloading:true})
      switch (route) {
          case "0":
              this.setState({generationText:"First Generation"})
              break;
          case "1":
              this.setState({generationText:"Second Generation"})
              break;
          case "2":
              this.setState({generationText:"Third Generation"})
              break;
          case "3":
              this.setState({generationText:"Fourth Generation"})
              break;
          case "4":
              this.setState({generationText:"Fifth Generation"})
              break;
          default:
            this.setState({generationText:"First Generation"})

      }

      const TOKEN = localStorage.getItem('token')
      let url = "https://api.adloyaltybn.com/site"
      axios({
        method: 'get',
        url: url,
        headers: {
          'x-access-token': TOKEN,
          'Content-Type': 'application/json'
      }
        //data:{ customer_id: global.id, payment_mode: paymentMode, address_id:this.props.address, expected_delivery_date:this.props.delivery_date, total:this.props.total, discount:this.props.discount, sub_total:this.props.sub_total, promo_id:this.props.promo_id, items: JSON.stringify(Object.values(this.props.items)) }
      })
       .then(res => {
		  this.setState({isloading:false})
           if(res.data.status == 1){
			   
              this.setState({
                  firstgeneration:res.data.result.first,
                  secondgeneration:res.data.result.second,
                  thirdgeneration:res.data.result.third,
                  fourthgeneration:res.data.result.fourth,
                  fifthgeneration:res.data.result.fifth,
              })
           }else{
               //this.setState({refName:res.data.result.l_name+" "+res.data.result.f_name,refId:res.data.result.id,gen:res.data.result.id+","+res.data.result.gen})
           }
       })

       axios({
         method: 'get',
         url: url+"/getteammate?downline="+route,
         headers: {
           'x-access-token': TOKEN,
           'Content-Type': 'application/json'
       }
         //data:{ customer_id: global.id, payment_mode: paymentMode, address_id:this.props.address, expected_delivery_date:this.props.delivery_date, total:this.props.total, discount:this.props.discount, sub_total:this.props.sub_total, promo_id:this.props.promo_id, items: JSON.stringify(Object.values(this.props.items)) }
       })
        .then(res => {
		   this.setState({isloading:true})
            if(res.data.status == 1){

               this.setState({
                   generationData:res.data.result,
               })
            }else{
                //this.setState({refName:res.data.result.l_name+" "+res.data.result.f_name,refId:res.data.result.id,gen:res.data.result.id+","+res.data.result.gen})
            }
		   this.setState({isloading:false})
        })
  }


  render () {
    return (
		<>
		{this.state.isloading ?
			<Spinner /> :
      <div>

        <div className="row page-title-header">
          <div className="col-12">
            <div className="page-header">
              <h4 className="page-title">Downline</h4>
            </div>
          </div>

        </div>
        <div className="row">
          <div className="col-md-12 grid-margin">
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col-xl-3 col-lg-6 col-sm-6 grid-margin-xl-0 grid-margin">
                    <div className="d-flex">
                      <div className="wrapper">
                        <h3 className="mb-0 font-weight-semibold">{this.state.firstgeneration}</h3>
                        <h5 className="mb-0 font-weight-medium text-primary">First Generation</h5>
                      </div>

                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-6 col-sm-6 mt-md-0 mt-4 grid-margin-xl-0 grid-margin">
                    <div className="d-flex">
                      <div className="wrapper">
                        <h3 className="mb-0 font-weight-semibold">{this.state.secondgeneration}</h3>
                        <h5 className="mb-0 font-weight-medium text-primary">Second Generation</h5>
                      </div>

                    </div>
                  </div>
                  <div className="col-xl-2 col-lg-6 col-sm-6 mt-md-0 mt-4 grid-margin-xl-0 grid-margin">
                    <div className="d-flex">
                      <div className="wrapper">
                        <h3 className="mb-0 font-weight-semibold">{this.state.thirdgeneration}</h3>
                        <h5 className="mb-0 font-weight-medium text-primary">Third Generation</h5>

                      </div>

                    </div>
                  </div>

                  <div className="col-xl-2 col-lg-6 col-sm-6 mt-md-0 mt-4 grid-margin-xl-0 grid-margin">
                    <div className="d-flex">
                      <div className="wrapper">
                        <h3 className="mb-0 font-weight-semibold">{this.state.fourthgeneration}</h3>
                        <h5 className="mb-0 font-weight-medium text-primary">Fourth Generation</h5>

                      </div>

                    </div>
                  </div>

                  <div className="col-xl-2 col-lg-6 col-sm-6 mt-md-0 mt-4 grid-margin-xl-0 grid-margin">
                    <div className="d-flex">
                      <div className="wrapper">
                        <h3 className="mb-0 font-weight-semibold">{this.state.fifthgeneration}</h3>
                        <h5 className="mb-0 font-weight-medium text-primary">Fifth Generation</h5>

                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title mb-0">{this.state.generationText}</h4>
                <div className="d-xl-flex flex-column flex-lg-row">
                {
                    this.state.generationData.length > 0 ?
                    <div className="table-responsive">
                      <table className="table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile No</th>
                            <th>Action</th>
                            <th>REF Link</th>
                          </tr>
                        </thead>
                        <tbody>
                    {this.state.generationData.map((value, index) => (


                              <tr key={index}>
                                <td>{value.l_name+" "+value.l_name}</td>
                                <td>{value.email}</td>
                                <td>{value.phones}</td>
                                <td><Link to={"/downlinedetails/"+value.id}><label className="badge badge-primary">View</label></Link></td>
                                <td>{"http://portal.adloyalty.com/"+value.ref_code}</td>

                              </tr>


                ))}
                </tbody>
                </table>
              </div>
                :
                null
            }

                </div>


            </div>
            </div>
          </div>

        </div>


      </div>
	}
	</>
    );
  }
}
export default Downline;
