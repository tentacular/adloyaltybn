import React, { Component } from 'react';
// import { Line, Doughnut, Bar, Radar } from 'react-chartjs-2';
import { Line, Bar, Radar } from 'react-chartjs-2';

import { ProgressBar, Dropdown } from 'react-bootstrap';
import GaugeChart from 'react-gauge-chart';
import { VectorMap } from "react-jvectormap"
import axios from 'axios';
import { Link} from 'react-router-dom';
import Spinner from '../shared/Spinner';


// import DatePicker from 'react-datepicker';
// import { Dropdown } from 'react-bootstrap';

export class Downlinedetails extends Component {



  constructor(props) {
    super(props);
    this.state = {
        firstgeneration:"",
        secondgeneration:"",
        thirdgeneration:"",
        fourthgeneration:"",
        fifthgeneration:"",
        generationData:[],
		isloading:false,
    }

  }


  componentDidUpdate(prevProps) {
      const { match: { params: { id } } } = this.props
      if (prevProps.match.params.id !== id){
		  this.setState({isloading:true})
          let route = id
          const TOKEN = localStorage.getItem('token')
          let url = "https://api.adloyaltybn.com/site"
           axios({
             method: 'get',
             url: url+"/getteammatedetails?id="+route,
             headers: {
               'x-access-token': TOKEN,
               'Content-Type': 'application/json'
           }
             //data:{ customer_id: global.id, payment_mode: paymentMode, address_id:this.props.address, expected_delivery_date:this.props.delivery_date, total:this.props.total, discount:this.props.discount, sub_total:this.props.sub_total, promo_id:this.props.promo_id, items: JSON.stringify(Object.values(this.props.items)) }
           })
            .then(res => {

                console.log(res.data.result)
				this.setState({isloading:false})
                if(res.data.status == 1){

                   this.setState({
                       generationData:res.data.result,
                   })
                }else{
                    //this.setState({refName:res.data.result.l_name+" "+res.data.result.f_name,refId:res.data.result.id,gen:res.data.result.id+","+res.data.result.gen})
                }
            })
      }
    }

  componentDidMount(){
      let route = this.props.match.params.id
	  this.setState({isloading:true})

      const TOKEN = localStorage.getItem('token')
      let url = "https://api.adloyaltybn.com/site"

       axios({
         method: 'get',
         url: url+"/getteammatedetails?id="+route,
         headers: {
           'x-access-token': TOKEN,
           'Content-Type': 'application/json'
       }
         //data:{ customer_id: global.id, payment_mode: paymentMode, address_id:this.props.address, expected_delivery_date:this.props.delivery_date, total:this.props.total, discount:this.props.discount, sub_total:this.props.sub_total, promo_id:this.props.promo_id, items: JSON.stringify(Object.values(this.props.items)) }
       })
        .then(res => {
            console.log(res.data.result)
			this.setState({isloading:false})
            if(res.data.status == 1){

               this.setState({
                   generationData:res.data.result,
               })
            }else{
                //this.setState({refName:res.data.result.l_name+" "+res.data.result.f_name,refId:res.data.result.id,gen:res.data.result.id+","+res.data.result.gen})
            }
        })
  }


  render () {
    return (
		<>
		{this.state.isloading ?
			<Spinner /> :
      <div>

      {Object.keys(this.state.generationData).length > 0 ?
          <>
        <div className="row page-title-header">
          <div className="col-12">
            <div className="page-header">
              <h4 className="page-title">Downline Details</h4>
            </div>
          </div>

        </div>
        <div className="row">
          <div className="col-md-12 grid-margin">
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col-xl-4 col-lg-6 col-sm-6 grid-margin-xl-0 grid-margin">
                    <div className="d-flex">
                      <div className="wrapper">
                        <h3 className="mb-0 font-weight-semibold">{this.state.generationData.consultantdetails.l_name+" "+this.state.generationData.consultantdetails.f_name}</h3>
                        <h5 className="mb-0 font-weight-medium text-primary">Consultant Name</h5>
                      </div>

                    </div>
                  </div>
                  <div className="col-xl-4 col-lg-6 col-sm-6 mt-md-0 mt-4 grid-margin-xl-0 grid-margin">
                    <div className="d-flex">
                      <div className="wrapper">
                        <h3 className="mb-0 font-weight-semibold">{this.state.generationData.totaldownline}</h3>
                        <h5 className="mb-0 font-weight-medium text-primary">Total Downline</h5>
                      </div>

                    </div>
                  </div>
                  <div className="col-xl-4 col-lg-6 col-sm-6 mt-md-0 mt-4 grid-margin-xl-0 grid-margin">
                    <div className="d-flex">
                      <div className="wrapper">
                        <h5 className="mb-0 ">{"http://portal.adloyalty.com/"+this.state.generationData.consultantdetails.ref_code}</h5>
                        <h5 className="mb-0 font-weight-medium text-primary">REF LINK</h5>

                      </div>

                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title mb-0">First Generation</h4>
                <div className="d-xl-flex flex-column flex-lg-row">
                {
                    this.state.generationData.gen1.length > 0 ?
                    <div className="table-responsive">
                      <table className="table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile No</th>
                            <th>Action</th>
                            <th>REF Link</th>
                          </tr>
                        </thead>
                        <tbody>
                    {this.state.generationData.gen1.map((value, index) => (


                              <tr key={index}>
                                <td>{value.l_name+" "+value.l_name}</td>
                                <td>{value.email}</td>
                                <td>{value.phones}</td>
                                <td><Link to={"/downlinedetails/"+value.id}><label className="badge badge-primary">View</label></Link></td>
                                <td>{"http://portal.adloyalty.com/"+value.ref_code}</td>

                              </tr>


                ))}
                </tbody>
                </table>
              </div>
                :
                <h4 className="card-title mb-0">You presently have no first generation</h4>
            }

                </div>


            </div>
            </div>
          </div>

        </div>

        <div className="row">
          <div className="col-md-12 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title mb-0">Second Generation</h4>
                <div className="d-xl-flex flex-column flex-lg-row">
                {
                    this.state.generationData.gen2.length > 0 ?
                    <div className="table-responsive">
                      <table className="table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile No</th>
                            <th>Action</th>
                            <th>REF Link</th>
                          </tr>
                        </thead>
                        <tbody>
                    {this.state.generationData.gen2.map((value, index) => (


                              <tr key={index}>
                                <td>{value.l_name+" "+value.l_name}</td>
                                <td>{value.email}</td>
                                <td>{value.phones}</td>
                                <td><Link to={"/downlinedetails/"+value.id}><label className="badge badge-primary">View</label></Link></td>
                                <td>{"http://portal.adloyalty.com/"+value.ref_code}</td>

                              </tr>


                ))}
                </tbody>
                </table>
              </div>
                :
                <h4 className="card-title mb-0">You presently dont have a second generation</h4>
            }

                </div>


            </div>
            </div>
          </div>

        </div>

        <div className="row">
          <div className="col-md-12 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title mb-0">Third Generation</h4>
                <div className="d-xl-flex flex-column flex-lg-row">
                {
                    this.state.generationData.gen3.length > 0 ?
                    <div className="table-responsive">
                      <table className="table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile No</th>
                            <th>Action</th>
                            <th>REF Link</th>
                          </tr>
                        </thead>
                        <tbody>
                    {this.state.generationData.gen3.map((value, index) => (


                              <tr key={index}>
                                <td>{value.l_name+" "+value.l_name}</td>
                                <td>{value.email}</td>
                                <td>{value.phones}</td>
                                <td><Link to={"/downlinedetails/"+value.id}><label className="badge badge-primary">View</label></Link></td>
                                <td>{"http://portal.adloyalty.com/"+value.ref_code}</td>

                              </tr>


                ))}
                </tbody>
                </table>
              </div>
                :
                <h4 className="card-title mb-0">You presently have no third Generation</h4>
            }

                </div>


            </div>
            </div>
          </div>

        </div>

        <div className="row">
          <div className="col-md-12 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title mb-0">Forth Generation</h4>
                <div className="d-xl-flex flex-column flex-lg-row">
                {
                    this.state.generationData.gen4.length > 0 ?
                    <div className="table-responsive">
                      <table className="table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile No</th>
                            <th>Action</th>
                            <th>REF Link</th>
                          </tr>
                        </thead>
                        <tbody>
                    {this.state.generationData.gen4.map((value, index) => (


                              <tr key={index}>
                                <td>{value.l_name+" "+value.l_name}</td>
                                <td>{value.email}</td>
                                <td>{value.phones}</td>
                                <td><Link to={"/downlinedetails/"+value.id}><label className="badge badge-primary">View</label></Link></td>
                                <td>{"http://portal.adloyalty.com/"+value.ref_code}</td>

                              </tr>


                ))}
                </tbody>
                </table>
              </div>
                :
                <h4 className="card-title mb-0">You presently have no fourth  generation</h4>
            }

                </div>


            </div>
            </div>
          </div>

        </div>

        <div className="row">
          <div className="col-md-12 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title mb-0">Fifth Generation</h4>
                <div className="d-xl-flex flex-column flex-lg-row">
                {
                    this.state.generationData.gen5.length > 0 ?
                    <div className="table-responsive">
                      <table className="table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile No</th>
                            <th>Action</th>
                            <th>REF Link</th>
                          </tr>
                        </thead>
                        <tbody>
                    {this.state.generationData.gen5.map((value, index) => (


                              <tr key={index}>
                                <td>{value.l_name+" "+value.l_name}</td>
                                <td>{value.email}</td>
                                <td>{value.phones}</td>
                                <td><Link to={"/downlinedetails/"+value.id}><label className="badge badge-primary">View</label></Link></td>
                                <td>{"http://portal.adloyalty.com/"+value.ref_code}</td>

                              </tr>


                ))}
                </tbody>
                </table>
              </div>
                :
                <h4 className="card-title mb-0">You presently have no fifth generation</h4>
            }

                </div>


            </div>
            </div>
          </div>

        </div>
        </>

        :null}

      </div>
	  }
	</>

    );
  }
}
export default Downlinedetails;
