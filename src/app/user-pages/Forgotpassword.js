import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Form } from 'react-bootstrap';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
//import { makeStyles,withStyles, } from '@material-ui/core/styles';

export class Forgotpassword extends Component {

    constructor(props) {
    super(props);
    this.state = {
        email:"",
        emailError:true,
        errorMessage:"",
        enternewpassword:false,
        password:"",
        token:"",
        stage:0,
		loaderStatus:false,
    }
    this.processforgotpassword = this.processforgotpassword.bind(this)
    this.onChangeEmail = this.onChangeEmail.bind(this)
    this.onChangePassword = this.onChangePassword.bind(this)
    this.processSetNewPassword = this.processSetNewPassword.bind(this)
    }
    componentDidMount(){
        var token = window.location.href.split("=")


        if(token.length > 1){
            this.setState({stage:2,token:token[1]})
        }

    }


   async processforgotpassword () {

       this.setState({errorMessage:"",emailError:true,passwordError:true,loaderStatus:true})

       if(this.state.email.length == 0){
           await this.setState({emailError:false,loaderStatus:false})
       }

       if (this.state.emailError == false) {
           this.setState({errorMessage:"Email Cant be empty",loaderStatus:false})
           return false;
       }else{
           // do an actual login and api call
           const requestForgotenPasswordData  = {
               email: this.state.email,
       };

            await axios.post('https://api.adloyaltybn.com/site/requestpasswordreset',  requestForgotenPasswordData)
             .then(res => {
				this.setState({loaderStatus:false})
				
                 if(res.data.status == 1){
                     this.setState({stage:1})
                 }else{
                     this.setState({errorMessage:"Email is wrong"})
                     console.log(res.data)
                 }
             })
       }

  }
   onChangeEmail (e) {
      this.setState({email:e.target.value})
  }


  async processSetNewPassword () {

      this.setState({errorMessage:"",passwordError:true,loaderStatus:true})

      if(this.state.password.length == 0){
          await this.setState({passwordError:false,loaderStatus:false})
      }

      if (this.state.passwordError == false) {
          this.setState({errorMessage:"Password Cant be empty",loaderStatus:false})
          return false;
      }else{
          // do an actual login and api call
          const resetPasswordData  = {
              password: this.state.password,
              otp: this.state.token,
      };

           await axios.post('https://api.adloyaltybn.com/site/resetpassword',  resetPasswordData)
            .then(res => {
			   this.setState({loaderStatus:false})
			   
                if(res.data.status == 1){
                    this.setState({stage:3})
                }else{
                    this.setState({errorMessage:"Something went wrong , Please try again. "})
                }
            })
      }

 }
  onChangePassword (e) {
     this.setState({password:e.target.value})
 }



  render() {
    return (
      <div>
        <div className="d-flex align-items-center auth px-0">
          <div className="row w-100 mx-0">
            <div className="col-lg-4 mx-auto">
              <div className="auth-form-light text-left py-5 px-4 px-sm-5">
                <div className="brand-logo">
                  <img src={require("../../assets/images/logo.png")} alt="logo" />
                </div>
                { this.state.stage == 0?
                <div>
                <h4>Hello! Forgot your Password ?</h4>
                <h5 style={useStyles.backgroundError}>{this.state.errorMessage}</h5>
                <h6 className="font-weight-light">Enter email to get started.</h6>
                <Form className="pt-3">
                  <Form.Group className="d-flex search-field">
                    <Form.Control type="email" placeholder="Email" size="lg" className="h-auto"  onChange={this.onChangeEmail} style={this.state.emailError == false ? useStyles.inputeError:null }/>
                  </Form.Group>
                  <div className="mt-3">
                   
					
					{ this.state.loaderStatus ?
						
						<button type="button" className="btn btn-block  btn-lg font-weight-medium auth-form-btn btn-warning"><CircularProgress style={{color:"white"}}  size={24}  /></button>
					  	:
						<button onClick={this.processforgotpassword} type="button" className="btn btn-block  btn-lg font-weight-medium auth-form-btn btn-warning">Proceed</button>
					  }
                  </div>
                  <div className="my-2 d-flex justify-content-between align-items-center">
                    <div className="form-check">

                    </div>
                  </div>

                </Form>
                </div>
                :null
                }

                { this.state.stage == 1?
                <div>
                <h4>An Email has been sent with a link, please check your mail box or spam box and follow the instructions.</h4>
                <div className="mt-3">
                  <Link to="/login" className="btn btn-block  btn-lg font-weight-medium auth-form-btn btn-warning">SIGN IN</Link>
                </div>
                </div>
                :null
                }

                { this.state.stage == 2?
                <div>

                <h5 style={useStyles.backgroundError}>{this.state.errorMessage}</h5>
                <h6 className="font-weight-light">Enter New Password.</h6>
                <Form className="pt-3">
                  <Form.Group className="d-flex search-field">
                    <Form.Control type="password" placeholder="Enter Password" size="lg" className="h-auto"  onChange={this.onChangePassword} style={this.state.passwordError == false ? useStyles.inputeError:null }/>
                  </Form.Group>
                  <div className="mt-3">
                    
					
					{ this.state.loaderStatus ?
						
						<button type="button" className="btn btn-block  btn-lg font-weight-medium auth-form-btn btn-warning"><CircularProgress style={{color:"white"}}  size={24}  /></button>
					  	:
						<button onClick={this.processSetNewPassword} type="button" className="btn btn-block  btn-lg font-weight-medium auth-form-btn btn-warning">Proceed</button>
					  }
                  </div>
                  <div className="my-2 d-flex justify-content-between align-items-center">
                    <div className="form-check">

                    </div>
                  </div>

                </Form>
                </div>
                :null
                }


                { this.state.stage == 3?
                <div>
                <h4>New Password have been set, go to login page to login.</h4>
                <div className="mt-3">
                  <Link to="/login" className="btn btn-block  btn-lg font-weight-medium auth-form-btn btn-warning">SIGN IN</Link>
                </div>
                </div>
                :null
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const useStyles = {
  root: {


  },
  backgroundError:{
    color:"red"
  },
  inputeError:{
    borderWidth:1,
    borderColor:"red"
  },


};

export default Forgotpassword
