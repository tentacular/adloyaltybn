import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Form } from 'react-bootstrap';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
//import { makeStyles,withStyles, } from '@material-ui/core/styles';

export class Login extends Component {

    constructor(props) {
    super(props);
    this.state = {
        email:"",
        password:"",
        emailError:true,
        passwordError:true,
        errorMessage:"",
		loaderStatus:false,

    }
    this.processLogin = this.processLogin.bind(this)
    this.onChangeEmail = this.onChangeEmail.bind(this)
    this.onChangePassword = this.onChangePassword.bind(this)

    }
    componentDidMount(){
		let route = this.props.match.params.id
		
		if(this.props.match.params.id !== undefined && Number.isInteger(parseInt(this.props.match.params.id) )){
			
			window.location = '/register?ref='+this.props.match.params.id;
		}
        if ("token" in localStorage) {
            window.location = '/dashboard';
        }
    }
      


   processLogin () {

       this.setState({errorMessage:"",emailError:true,passwordError:true,loaderStatus:true})

       if(this.state.email.length == 0){
           this.setState({emailError:false,loaderStatus:false})
       }

       if(this.state.password.length == 0){
           this.setState({passwordError:false,loaderStatus:false})
       }

       if(this.state.emailError == false && this.state.passwordError == false){
           this.setState({errorMessage:"Email and Password can not be empty"})
           return false;
       }else if (this.state.emailError == false) {
           this.setState({errorMessage:"Email Cant be empty"})
           return false;
       }else if (this.state.passwordError == false) {
           this.setState({errorMessage:"Password cant be empty"})
           return false;
       }else{
           // do an actual login and api call
           const loginData  = {
               password: this.state.password,
               username: this.state.email,
       };

            axios.post('https://api.adloyaltybn.com/site/login',  loginData)
             .then(res => {
				this.setState({loaderStatus:false})
				
                 if(res.data.status == 1){
                     localStorage.setItem('token', res.data.result.access_token);
                     localStorage.setItem('name', res.data.result.l_name+" "+res.data.result.f_name);
                     localStorage.setItem('reff', res.data.result.ref_code);
                     window.location = '/dashboard';
                 }else{
                     this.setState({errorMessage:"Email or Password is wrong"})
                 }
             })
       }

  }
   onChangeEmail (e) {
      this.setState({email:e.target.value})
  }


  onChangePassword (e) {
     this.setState({password:e.target.value})
 }

  render() {
    return (
      <div>
        <div className="d-flex align-items-center auth px-0">
          <div className="row w-100 mx-0">
            <div className="col-lg-4 mx-auto">
              <div className="auth-form-light text-left py-5 px-4 px-sm-5">
                <div className="brand-logo">
                  <img src={require("../../assets/images/logo.png")} alt="logo" />
                </div>
                <h4>Hello! let's get started</h4>
                <h5 style={useStyles.backgroundError}>{this.state.errorMessage}</h5>
                <h6 className="font-weight-light">Sign in to continue.</h6>
                <Form className="pt-3">
                  <Form.Group className="d-flex search-field">
                    <Form.Control type="email" placeholder="Email" size="lg" className="h-auto"  onChange={this.onChangeEmail} style={this.state.emailError == false ? useStyles.inputeError:null }/>
                  </Form.Group>
                  <Form.Group className="d-flex search-field">
                    <Form.Control type="password" placeholder="Password" size="lg" className="h-auto" onChange={this.onChangePassword} style={this.state.passwordError == false ? useStyles.inputeError:null }/>
                  </Form.Group>
                  <div className="mt-3">
					  { this.state.loaderStatus ?
					  	<button  type="button" className="btn btn-block  btn-lg font-weight-medium auth-form-btn btn-warning"><CircularProgress style={{color:"white"}}  size={24}  /> </button> :
						<button onClick={this.processLogin} type="button" className="btn btn-block  btn-lg font-weight-medium auth-form-btn btn-warning">SIGN IN </button>
					  }
                    
					
					
					
                  </div>
                  <div className="my-2 d-flex justify-content-between align-items-center">
                    <div className="form-check">

                    </div>
                    <Link to="/forgotpassword" className="auth-link text-black">Forgot password?</Link>
                  </div>

                  <div className="text-center mt-4 font-weight-light">
                    Don't have an account? <Link to="/register" className="text-primary">Create</Link>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const useStyles = {
  root: {


  },
  backgroundError:{
    color:"red"
  },
  inputeError:{
    borderWidth:1,
    borderColor:"red"
  },


};

export default Login
