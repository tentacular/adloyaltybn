import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Spinner from '../shared/Spinner';
import CircularProgress from '@material-ui/core/CircularProgress';
export class Register extends Component {

    constructor(props) {
    super(props);
    this.state = {
        refName:"",
        reffCode:"",
        refId:"",
        registrationStatus:false,
        gen:"",
        email:"",
        phone:"",
        password:"",
    	fname:"",
    	lname:"",
    	gender:"",
    	state:"",
    	country:"",
    	accountname:"",
    	accountnumber:"",
    	bankname:"",
        emailError:true,
        phoneError:true,
        passwordError:true,
    	fnameError:true,
    	lnameError:true,
    	genderError:true,
    	stateError:true,
    	countryError:true,
    	accountnameError:true,
    	accountnumberError:true,
    	banknameError:true,
        errorMessage:"",
		isloading:false,
		loaderStatus:false,

    }

    this.setEmail = this.setEmail.bind(this)
    this.setPhone = this.setPhone.bind(this)
    this.setPassword = this.setPassword.bind(this)
    this.setFname = this.setFname.bind(this)
    this.setLname = this.setLname.bind(this)
    this.setGender = this.setGender.bind(this)
    this.setStates = this.setStates.bind(this)
    this.setCountry = this.setCountry.bind(this)
    this.setAccountname = this.setAccountname.bind(this)
    this.setBankname = this.setBankname.bind(this)
    this.setAccountnumber = this.setAccountnumber.bind(this)
    this.processRegistration = this.processRegistration.bind(this)

    }

    setEmail(e){
        this.setState({email:e.target.value})
    }

    setPhone(e){
        this.setState({phone:e.target.value})
    }

    setPassword(e){
        this.setState({password:e.target.value})
    }

    setFname(e){
        this.setState({fname:e.target.value})
    }
    setLname(e){
        this.setState({lname:e.target.value})
    }
    setGender(e){
        this.setState({gender:e.target.value})
    }
    setStates(e){
        this.setState({state:e.target.value})
    }
    setCountry(e){
        this.setState({country:e.target.value})
    }
    setAccountname(e){
        this.setState({accountname:e.target.value})
    }
    setBankname(e){
        this.setState({bankname:e.target.value})
    }
    setAccountnumber(e){
        this.setState({accountnumber:e.target.value})
    }

    componentDidMount(){
		this.setState({isloading:true})
        var ref = window.location.href.split("=")
        var url = "";

        if(ref.length > 1){
            url = "https://api.adloyaltybn.com/site/getrefregistration?id="+ref[1]
        }else{
            url = "https://api.adloyaltybn.com/site/getrefregistration"
        }
        axios.get(url)
         .then(res => {
			this.setState({isloading:false})
             if(res.data.status == 1){
                console.log(res.data)
                this.setState({refName:res.data.result.f_name+" "+res.data.result.l_name,refId:res.data.result.id,gen:res.data.result.id+","+res.data.result.gen})
             }else{
                 this.setState({refName:res.data.result.l_name+" "+res.data.result.f_name,refId:res.data.result.id,gen:res.data.result.id+","+res.data.result.gen})
             }
         })
    }

    processRegistration(){
        this.setState({errorMessage:"",emailError:true,phoneError:true,passwordError:true,fnameError:true,lnameError:true,genderError:true,stateError:true,countryError:true,accountnameError:true,accountnumberError:true,banknameError:true,loaderStatus:true})

        if(this.state.email.length == 0){
            this.setState({emailError:false,loaderStatus:false})
        }

        if(this.state.password.length == 0){
            this.setState({passwordError:false,loaderStatus:false})
        }
        if(this.state.phone.length == 0){
            this.setState({phoneError:false,loaderStatus:false})
        }
        if(this.state.fname.length == 0){
            this.setState({fnameError:false,loaderStatus:false})
        }
        if(this.state.lname.length == 0){
            this.setState({lnameError:false,loaderStatus:false})
        }
        if(this.state.gender.length == 0){
            this.setState({genderError:false,loaderStatus:false})
        }
        if(this.state.state.length == 0){
            this.setState({stateError:false,loaderStatus:false})
        }
        if(this.state.country.length == 0){
            this.setState({countryError:false,loaderStatus:false})
        }
        if(this.state.accountname.length == 0){
            this.setState({accountnameError:false,loaderStatus:false})
        }
        if(this.state.accountnumber.length == 0){
            this.setState({accountnumberError:false,loaderStatus:false})
        }
        if(this.state.bankname.length == 0){
            this.setState({banknameError:false,loaderStatus:false})
        }

        if(this.state.emailError == false && this.state.passwordError == false
            && this.state.phoneError == false
            && this.state.fnameError == false
            && this.state.lnameError == false
            && this.state.genderError == false
            && this.state.stateError == false
            && this.state.countryError == false
            && this.state.accountnameError == false
            && this.state.accountnumberError == false
            && this.state.banknameError == false
        ){
            this.setState({errorMessage:"Form can not be empty"})
            return false;
        }else if (this.state.emailError == false) {
            this.setState({errorMessage:"Email can not be empty"})
            return false;
        }else if (this.state.passwordError == false) {
            this.setState({errorMessage:"Password can not be empty"})
            return false;
        }else if (this.state.phoneError == false) {
            this.setState({errorMessage:"Phone can not be empty"})
            return false;
        }else if (this.state.fnameError == false) {
            this.setState({errorMessage:"First name can not be empty"})
            return false;
        }else if (this.state.lnameError == false) {
            this.setState({errorMessage:"Last name can not be empty"})
            return false;
        }else if (this.state.genderError == false) {
            this.setState({errorMessage:"Gender can not be empty"})
            return false;
        }else if (this.state.stateError == false) {
            this.setState({errorMessage:"State can not be empty"})
            return false;
        }else if (this.state.countryError == false) {
            this.setState({errorMessage:"Country can not be empty"})
            return false;
        }else if (this.state.accountnameError == false) {
            this.setState({errorMessage:"Account name can not be empty"})
            return false;
        }else if (this.state.accountnumberError == false) {
            this.setState({errorMessage:"Account number can not be empty"})
            return false;
        }else if (this.state.banknameError == false) {
            this.setState({errorMessage:"Bank name can not be empty"})
            return false;
        }else{
            // do an actual login and api call
            const registrationData  = {
                password: this.state.password,
                email: this.state.email,
                refid:this.state.refId,
                gen:this.state.gen,
                phones:this.state.phone,
            	fname:this.state.fname,
            	lname:this.state.lname,
            	gender:this.state.gender,
            	state:this.state.state,
            	country:this.state.country,
            	accountname:this.state.accountname,
            	accountnumber:this.state.accountnumber,
            	bankname:this.state.bankname,
        };

             axios.post('https://api.adloyaltybn.com/site/register',  registrationData)
              .then(res => {
                  console.log(res.data)
                  if(res.data.status == 1){

                      this.setState({registrationStatus:true,reffCode:res.data.result.ref_code,loaderStatus:false})

                  }else{
                      this.setState({errorMessage:"something went wrong, please retry later.",loaderStatus:false})
                  }
              })
        }
    }


  render() {
    return (
		
      <div>
		{this.state.isloading ? <Spinner />:
        <div className="d-flex align-items-center auth px-0">
          <div className="row w-100 mx-0">
            <div className="col-lg-4 mx-auto">
              <div className="auth-form-light text-left py-5 px-4 px-sm-5">
                <div className="brand-logo">
                  <img src={require("../../assets/images/logo.png")} alt="logo" />
                </div>
                {this.state.registrationStatus?
                    <div>

                    <h2 style={{color:"green"}}>CONGRATULATIONS </h2>

                    <h5>Your Adloyalty Business Network Registration is Successful. </h5>

                    <h6 >Your Refferal Link is <em style={{color:"red"}}>{"www.portal.adloyaltybn.com/register?ref="+this.state.reffCode}</em> </h6>

                    <div className="form-group">
                        <ul>
                        <h4 style={{color:"green"}}>TO FAST-TRACK YOUR SUCCESS IN THIS REAL ESTATE BUSINESS; kindly </h4>

                        <li>Join ADLOYALTYBN OFFICIAL WHAT'S APP GROUP by messaging 09085822222 on WHATS APP.</li>

                        <li>BUY THE BOOK: PRINT MONEY WITH ZERO CAPITAL THE NO-NONSENSE GUIDE TO REAL ESTATE MONEY SPINNING BUSINESS.</li>

                        <h5 style={{color:"red"}}>This Book is Nigeria's very first, ultimate and most comprehensive guide for success in Real Estate Brokerage.</h5>
                        </ul>


                        <a href="http://www.freemanosonuga.com/dfobooks" className="btn btn-block  btn-lg font-weight-medium auth-form-btn btn-warning">Order Now</a>
                    </div>

                    <div className="mt-3">
                      <Link to="/login" className="btn btn-block  btn-lg font-weight-medium auth-form-btn btn-primary">SIGN IN </Link>
                    </div>

                    </div>
                    :
                <>
                <h4>New here?</h4>
                <h5>REFERED BY: {this.state.refName}</h5>
                <h5 style={useStyles.backgroundError}>{this.state.errorMessage}</h5>
                <h6 className="font-weight-light">Signing up is easy. It only takes a few steps</h6>
                <form className="pt-3">
                  <div className="form-group">
                    <input type="text" className="form-control form-control-lg" style={this.state.fnameError == false ? useStyles.inputeError:null } onChange={this.setFname}  placeholder="First Name" />
                  </div>

                  <div className="form-group">
                    <input type="tel" className="form-control form-control-lg" style={this.state.lnameError == false ? useStyles.inputeError:null } onChange={this.setLname}  placeholder="Last Name" />
                  </div>

                  <div className="form-group">
                    <input type="email" className="form-control form-control-lg" style={this.state.emailError == false ? useStyles.inputeError:null } onChange={this.setEmail}  placeholder="Email" />
                  </div>

                  <div className="form-group">
                    <input type="text" className="form-control form-control-lg" style={this.state.phoneError == false ? useStyles.inputeError:null } onChange={this.setPhone}  placeholder="Phone Number" />
                  </div>

                  <div className="form-group">
                    <select className="form-control form-control-lg" style={this.state.genderError == false ? useStyles.selectError:null } onChange={this.setGender} id="exampleFormControlSelect2">
                      <option>Gender</option>
                      <option value="1">Male</option>
                      <option value="2">Female</option>
                    </select>
                  </div>

                  <div className="form-group">
                    <input type="text" className="form-control form-control-lg" style={this.state.stateError == false ? useStyles.inputeError:null } onChange={this.setStates}  placeholder="State" />
                  </div>

                  <div className="form-group">
                    <input type="text" className="form-control form-control-lg" style={this.state.countryError == false ? useStyles.inputeError:null } onChange={this.setCountry}  placeholder="Country" />
                  </div>

                  <div className="form-group">
                    <input type="text" className="form-control form-control-lg" style={this.state.banknameError == false ? useStyles.inputeError:null } onChange={this.setBankname}  placeholder="Bank" />
                  </div>

                  <div className="form-group">
                    <input type="text" className="form-control form-control-lg" style={this.state.accountnameError == false ? useStyles.inputeError:null } onChange={this.setAccountname}  placeholder="Account Name" />
                  </div>

                  <div className="form-group">
                    <input type="text" className="form-control form-control-lg" style={this.state.accountnumberError == false ? useStyles.inputeError:null } onChange={this.setAccountnumber}  placeholder="Account Number" />
                  </div>

                  <div className="form-group">
                    <input type="password" className="form-control form-control-lg" style={this.state.passwordError == false ? useStyles.inputeError:null } onChange={this.setPassword} id="exampleInputPassword1" placeholder="Password" />
                  </div>

                  <div className="mt-3">
                    
		
		
					{ this.state.loaderStatus ?
					<button type="button" className="btn btn-block  btn-lg font-weight-medium auth-form-btn btn-warning"><CircularProgress style={{color:"white"}}  size={24}  /></button>
					  	:
						<button onClick={this.processRegistration} type="button" className="btn btn-block  btn-lg font-weight-medium auth-form-btn btn-warning">SIGN Up</button>
					  }
                  </div>
                  <div className="text-center mt-4 font-weight-light">
                    Already have an account? <Link to="/login" className="text-primary">Login</Link>
                  </div>
                </form>
                </>
            }

              </div>
            </div>
          </div>
        </div>
		}
      </div>
    )
  }
}

const useStyles = {
  root: {


  },
  backgroundError:{
    color:"red"
  },
  inputeError:{
    borderWidth:1,
    borderColor:"red"
  },
  selectError:{
    borderWidth:1,
    outlineColor:"red"
  },


};

export default Register
